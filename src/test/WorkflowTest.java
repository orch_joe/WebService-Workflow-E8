package test;

import org.junit.Test;

import weaver.workflow.webservices.WorkflowBaseInfo;
import weaver.workflow.webservices.WorkflowMainTableInfo;
import weaver.workflow.webservices.WorkflowRequestInfo;
import weaver.workflow.webservices.WorkflowRequestLog;
import weaver.workflow.webservices.WorkflowRequestTableField;
import weaver.workflow.webservices.WorkflowRequestTableRecord;
import cn.com.weaver.services.webservices.WorkflowServicePortTypeProxy;

public class WorkflowTest {
	private static final String          wsurl = "http://localhost:8080/services/WorkflowService";
	private WorkflowServicePortTypeProxy client;

	public WorkflowTest() {
		this.client = new WorkflowServicePortTypeProxy(wsurl);
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		WorkflowTest ws = new WorkflowTest();
		ws.createRequest();// 创建流程
		// ws.submitWorkflowRequest();//提交流程
		// ws.getToDoWorkflowRequestList();//获取用户待办列表
		// ws.getToDoWorkflowRequestCount();//获取用户待办总数
		// ws.getRequestLogs();
	}

	/**
	 * 创建流程
	 * 
	 * @throws Exception
	 */
	public void createRequest() throws Exception {
		// 主字段
		WorkflowRequestTableField[] wrti = new WorkflowRequestTableField[2]; // 字段信息
		/*
		 * wrti[0] = new WorkflowRequestTableField();
		 * wrti[0].setFieldName("mutiresource");// 被留言人
		 * wrti[0].setFieldValue("29");// 被留言人字段的值，111为被留言人id //
		 * wrti[0].setFieldValue("24");//被留言人字段的值，111为被留言人id
		 * wrti[0].setView(true);// 字段是否可见 wrti[0].setEdit(true);// 字段是否可编辑
		 * 
		 * wrti[1] = new WorkflowRequestTableField();
		 * wrti[1].setFieldName("remark");// 留言内容
		 * wrti[1].setFieldValue("tes't&abctes't&abctes't&abc");
		 * wrti[1].setView(true); wrti[1].setEdit(true);
		 * 
		 * wrti[2] = new WorkflowRequestTableField();
		 * wrti[2].setFieldName("resource_n");// 留言人
		 * wrti[2].setFieldValue("29"); // wrti[2].setFieldValue("24");
		 * wrti[2].setView(true); wrti[2].setEdit(true);
		 */

		wrti[0] = new WorkflowRequestTableField();
		// 附件字段名
		wrti[0].setFieldName("ACMtestfujscWj");
		// 附件名称
		wrti[0].setFieldType("http:baidu_sylogo1.gif,http:bbbbbbbb.jpg");
		// 附件下载地址
		wrti[0]
		        .setFieldValue("http://www.baidu.com/img/baidu_sylogo1.gif,http://e.hiphotos.baidu.com/image/w%3D310/sign=573d50299113b07ebdbd56093cd69113/cf1b9d16fdfaaf515f1156b78e5494eef11f7acc.jpg");// http:开头代表该字段为附件字段
		wrti[0].setView(true);
		wrti[0].setEdit(true);

		wrti[1] = new WorkflowRequestTableField();
		wrti[1].setFieldName("ACMtestfujscTp");// 附件2
		wrti[1].setFieldType("http:baidu_sylogo1.gif,http:bbbbbbbb.jpg");
		wrti[1]
		        .setFieldValue("http://www.baidu.com/img/baidu_sylogo1.gif,http://e.hiphotos.baidu.com/image/w%3D310/sign=573d50299113b07ebdbd56093cd69113/cf1b9d16fdfaaf515f1156b78e5494eef11f7acc.jpg");// http:开头代表该字段为附件字段
		wrti[1].setView(true);
		wrti[1].setEdit(true);

		WorkflowRequestTableRecord[] wrtri = new WorkflowRequestTableRecord[1];// 主字段只有一行数据
		wrtri[0] = new WorkflowRequestTableRecord();
		wrtri[0].setWorkflowRequestTableFields(wrti);

		WorkflowMainTableInfo wmi = new WorkflowMainTableInfo();
		wmi.setRequestRecords(wrtri);

		/*
		 * //明细字段 WorkflowDetailTableInfo wdti[] = new
		 * WorkflowDetailTableInfo[2];//两个明细表0明细表1,1明细表2
		 * 
		 * //明细表1 start wrtri = new WorkflowRequestTableRecord[2];//数据
		 * 行数，假设添加2行明细数据 //第一行 wrti = new WorkflowRequestTableField[3]; //每行3个字段
		 * wrti[0] = new WorkflowRequestTableField();
		 * wrti[0].setFieldName("sl");//数量 wrti[0].setFieldValue("1");
		 * wrti[0].setView(true); wrti[0].setEdit(true);
		 * 
		 * wrti[1] = new WorkflowRequestTableField();
		 * wrti[1].setFieldName("dj");//单价 wrti[1].setFieldValue("2");
		 * wrti[1].setView(true); wrti[1].setEdit(true);
		 * 
		 * wrti[2] = new WorkflowRequestTableField();
		 * wrti[2].setFieldName("xj");//小记 wrti[2].setFieldValue("3");
		 * wrti[2].setView(true); wrti[2].setEdit(true);
		 * 
		 * wrtri[0] = new WorkflowRequestTableRecord();
		 * wrtri[0].setWorkflowRequestTableFields(wrti);
		 * 
		 * //第二行 wrti = new WorkflowRequestTableField[3]; //每行3个字段 wrti[0] = new
		 * WorkflowRequestTableField(); wrti[0].setFieldName("sl");//数量
		 * wrti[0].setFieldValue("11"); wrti[0].setView(true);
		 * wrti[0].setEdit(true);
		 * 
		 * wrti[1] = new WorkflowRequestTableField();
		 * wrti[1].setFieldName("dj");//单价 wrti[1].setFieldValue("22");
		 * wrti[1].setView(true); wrti[1].setEdit(true);
		 * 
		 * wrti[2] = new WorkflowRequestTableField();
		 * wrti[2].setFieldName("xj");//小记 wrti[2].setFieldValue("33");
		 * wrti[2].setView(true); wrti[2].setEdit(true);
		 * 
		 * wrtri[1] = new WorkflowRequestTableRecord();
		 * wrtri[1].setWorkflowRequestTableFields(wrti);
		 * 
		 * wdti[0] = new WorkflowDetailTableInfo();
		 * wdti[0].setWorkflowRequestTableRecords(wrtri);//加入明细表1的数据 //明细表1 end
		 * 
		 * 
		 * //明细表2 start wrtri = new
		 * WorkflowRequestTableRecord[1];//数据行数，假设添加1行明细数据
		 * 
		 * //第一行 wrti = new WorkflowRequestTableField[3]; //每行2个字段 wrti[0] = new
		 * WorkflowRequestTableField(); wrti[0].setFieldName("cl3");//
		 * wrti[0].setFieldValue("1"); wrti[0].setView(true);
		 * wrti[0].setEdit(true);
		 * 
		 * wrti[1] = new WorkflowRequestTableField();
		 * wrti[1].setFieldName("cl1111");// wrti[1].setFieldValue("2");
		 * wrti[1].setView(true); wrti[1].setEdit(true);
		 * 
		 * wrtri[0] = new WorkflowRequestTableRecord();
		 * wrtri[0].setWorkflowRequestTableFields(wrti);
		 * 
		 * wdti[1] = new WorkflowDetailTableInfo();
		 * wdti[1].setWorkflowRequestTableRecords(wrtri);//加入明细表2的数据
		 */// 明细表2 end

		WorkflowBaseInfo wbi = new WorkflowBaseInfo();
		wbi.setWorkflowId("1875");// workflowid 5 代表内部留言

		WorkflowRequestInfo wri = new WorkflowRequestInfo();// 流程基本信息
		wri.setCreatorId("1000002");// 创建人id
		wri.setRequestLevel("2");// 0 正常，1重要，2紧急
		wri.setRequestName("测试多附件接口" + System.currentTimeMillis());// 流程标题
		wri.setWorkflowMainTableInfo(wmi);// 添加主字段数据
		wri.setWorkflowBaseInfo(wbi);
		// wri.setWorkflowDetailTableInfos(wdti);

		// WorkflowServiceLocator WorkflowServiceLocator = new
		// WorkflowServiceLocator();
		// WorkflowServicePortType WorkflowServicePortType =
		// WorkflowServiceLocator.getWorkflowServiceHttpPort();
		// System.out.println(WorkflowServicePortType.doCreateWorkflowRequest(wri,
		// 111));

		String requestid = client.doCreateWorkflowRequest(wri, 29);
		System.out.println("requestid:" + requestid);
	}
	
	
	
	

	/**
	 * 获取用户待办列表
	 * 
	 * @throws Exception
	 */
	public void getToDoWorkflowRequestList() throws Exception {
		int pageNo = 1; // 页号
		int pageSize = 10; // 每页记录数
		int userId = 1000002; // 用户id
		String[] conditions = null; // 查询条件

		int recordCount = getToDoWorkflowRequestCount(); // 总记录数
		WorkflowRequestInfo[] todoArray = client.getToDoWorkflowRequestList(pageNo, pageSize, recordCount, userId,
		        conditions);
		if (todoArray != null) {
			for (int i = 0; i < todoArray.length; i++) {
				WorkflowRequestInfo wri = todoArray[i];
				String requestid = wri.getRequestId();// 请求id
				String requestname = wri.getRequestName();// 请求标题
				String requestlevel = wri.getRequestLevel();
				String creator = wri.getCreatorId();// 创建人id
				String creatorName = wri.getCreatorName();// 创建人姓名
				String createTime = wri.getCreateTime();// 创建时间

			}
		}
	}

	/**
	 * 获取用户待办数量
	 * 
	 * @return
	 * @throws Exception
	 */
	public int getToDoWorkflowRequestCount() throws Exception {
		int userId = 1000002; // 用户id
		String[] conditions = null; // 查询条件
		return client.getToDoWorkflowRequestCount(userId, conditions);
	}

	@Test
	public void getRequestLogs() throws Exception {
		String workflowId = "41";
		int requestid = 1536266;
		int userid = 22175;
		int pagesize = 10;
		int endId = 0;

		WorkflowRequestInfo wri = client.getWorkflowRequest(requestid, userid, requestid);
		WorkflowRequestLog[] logs = wri.getWorkflowRequestLogs();

		// WorkflowRequestLog[] logs = client.getWorkflowRequestLogs(workflowId,
		// requestid + "", userid, pagesize, endId);
		for (int i = 0; i < logs.length; i++) {
			WorkflowRequestLog log = logs[i];
			String Remark = log.getRemark();
			String OperateDate = log.getOperateDate();
			String OperateTime = log.getOperateTime();
			String OperatorId = log.getOperatorId();
			String OperatorName = log.getOperatorName();

			System.out.println("OperatorId=" + OperatorId + ", OperatorName=" + OperatorName + ", Remark=" + Remark
			        + ", OperateDate=" + OperateDate + ", OperateTime=" + OperateTime);

		}

	}
}
