package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;

/**
 * 取得流程new标记
 * @author Administrator
 *
 */
public class GetWorkflowNewFlag {

	/**
	 * 取得流程new标记
	 * @param requestids
	 * @param resourceid
	 * @return
	 */
	@Test
	public void excute() throws RemoteException{

		String[] requestids =  new String[3];
		requestids[0] = "15456";
		requestids[1] = "15652";
		requestids[2] = "16854";
		String resourceid = "12545";
		String[] response = ClientUtil.getClient().getWorkflowNewFlag(requestids, resourceid);
		System.out.println("length:"+response.length);

		for (String str : response) {
			System.out.println("Flag:"+str);
		}
	}
}
