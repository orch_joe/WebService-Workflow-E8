package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;

import weaver.workflow.webservices.WorkflowRequestInfo;

/**
 * 取得创建流程的相关信息
 * @author zxd
 *
 */
public class GetCreateWorkflowRequestInfo {
	
	/**
	 * 取得创建流程的相关信息
	 * 
	 * @param workflowId 工作流ID
	 * @param userid 当前用户
	 * @return WorkflowRequestInfo 流程信息
	 */
	@Test
	public void excute() throws RemoteException{
		int workflowId = 14679;//工作流ID
		int userid = 1;//当前用户
		WorkflowRequestInfo  response = ClientUtil.getClient().getCreateWorkflowRequestInfo(workflowId, userid);

		if(response != null)
			System.out.println("requestid:"+response.getRequestId()+
					" -----"+"创建者姓名:"+response.getCreatorName()+
					" -----"+"级数:"+response.getRequestLevel()+
					" -----"+"是否可编辑:"+response.getCanEdit());
		else
			System.out.println("response is null....");
	}
}
