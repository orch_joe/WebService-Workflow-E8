package test.WorkflowServicePortType;
import java.rmi.RemoteException;
import org.junit.Test;
import cn.com.weaver.services.webservices.WorkflowServicePortTypeProxy;
/**
 * 获取抄送流程数量示例
 * @author Administrator
 *
 */
public class GetCCWorkflowRequestCount {
	private static final String WSURL = "http://localhost:8080/services/WorkflowService";
	private WorkflowServicePortTypeProxy client;

	public GetCCWorkflowRequestCount() {
		this.client = new WorkflowServicePortTypeProxy(WSURL);
	}
	
	/**
	 * 获取抄送流程数量
	 * 
	 * @param userid 当前用户
	 * @param conditions 查询条件
	 * @return int 抄送流程数量
	 */
	@Test
	public void excute() throws RemoteException{
		int userid = 1;//当前用户
		String[] conditions = new String[2];//查询条件
		
		int response = client.getCCWorkflowRequestCount(userid, conditions);
		
		System.out.println("response:"+response);
	}
}
