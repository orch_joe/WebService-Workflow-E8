package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;
/**
 * 取得可创建的工作流数量示例
 * @author Administrator
 *
 */
public class GetCreateWorkflowCount {
	
	/**
	 * 取得可创建的工作流数量
	 * 
	 * @param userid 当前用户
	 * @param workflowType 工作流类型
	 * @param conditions 查询条件
	 * @return int 工作流数量
	 */
	@Test
	public void excute() throws RemoteException{
		int userid = 1;//当前用户
		int workflowType = 0;//工作流类型
		String[] conditions = new String[2];//查询条件
		conditions[0] = "1";
		conditions[1] = "1";
		int response = ClientUtil.getClient().getCreateWorkflowCount(userid, workflowType, conditions);
		System.out.println("count:"+response);
	}

}
