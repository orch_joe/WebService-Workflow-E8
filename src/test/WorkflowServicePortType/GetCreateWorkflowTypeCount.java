package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;

/**
 * 取得可创建的工作流类型数量示例
 * @author zxd
 *
 */
public class GetCreateWorkflowTypeCount {
	
	/**
	 * 取得可创建的工作流类型数量
	 * 
	 * @param userid 当前用户
	 * @param conditions 查询条件
	 * @return int 工作流类型数量
	 */
	@Test
	public void excute() throws RemoteException{
		int userid = 1;//当前用户
		String[] conditions = new String[2];//查询条件
		conditions[0] = "";
		conditions[1] = "";
		int response = ClientUtil.getClient().getCreateWorkflowTypeCount(userid, conditions);
		System.out.println("response:"+response);

	}
}
