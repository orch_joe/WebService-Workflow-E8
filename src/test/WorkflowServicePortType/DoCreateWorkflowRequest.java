package test.WorkflowServicePortType;

import org.junit.Test;

import weaver.workflow.webservices.WorkflowBaseInfo;
import weaver.workflow.webservices.WorkflowDetailTableInfo;
import weaver.workflow.webservices.WorkflowMainTableInfo;
import weaver.workflow.webservices.WorkflowRequestInfo;
import weaver.workflow.webservices.WorkflowRequestTableField;
import weaver.workflow.webservices.WorkflowRequestTableRecord;
/**
 * 创建流程示例
 * @author zxd
 *
 */
public class DoCreateWorkflowRequest {

	/**
	 * 执行创建流程
	 * 
	 * @param WorkflowRequestInfo 流程信息
	 * @param userid 当前用户
	 * @return String 返回结果
	 */
	@Test
	public  void Createtest() throws Exception {

		WorkflowRequestInfo workflowRequestInfo = new WorkflowRequestInfo();//工作流程请求信息

		int userid = 115;//用户ID
//		workflowRequestInfo.setRequestId(String.valueOf(1918557));//流程请求ID-创建流程时自动产生 不需要传此项
		workflowRequestInfo.setCanView(true);//显示
		workflowRequestInfo.setCanEdit(true);//可编辑
		workflowRequestInfo.setRequestName("流程请求标题-webservice-test");//请求标题
		workflowRequestInfo.setRequestLevel("0");//请求重要级别 0：正常 1：重要 2：紧急
		workflowRequestInfo.setCreatorId("115");//创建者ID 创建流程时为必输项

		WorkflowBaseInfo workflowBaseInfo = new WorkflowBaseInfo();//工作流信息
		workflowBaseInfo.setWorkflowId("14679");//流程ID
		workflowBaseInfo.setWorkflowName("webservice-test");//流程名称
//		workflowBaseInfo.setWorkflowTypeId("1951");//流程类型id
		workflowBaseInfo.setWorkflowTypeName("webservice-test");//流程类型名称
		workflowRequestInfo.setWorkflowBaseInfo(workflowBaseInfo);//工作流信息



		/****************main table start*************/
		WorkflowMainTableInfo workflowMainTableInfo = new WorkflowMainTableInfo();//主表
		WorkflowRequestTableRecord[] workflowRequestTableRecord = new WorkflowRequestTableRecord[1];//主表字段只有一条记录
		WorkflowRequestTableField[] WorkflowRequestTableField = new WorkflowRequestTableField[6];//主的4个字段

		WorkflowRequestTableField[0] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[0].setFieldName("name");//姓名
		WorkflowRequestTableField[0].setFieldValue("115");//被留言人字段的值，111为被留言人id
		WorkflowRequestTableField[0].setView(true);//字段是否可见
		WorkflowRequestTableField[0].setEdit(true);//字段是否可编辑

		WorkflowRequestTableField[1] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[1].setFieldName("department");//部门
		WorkflowRequestTableField[1].setFieldValue("3");
		WorkflowRequestTableField[1].setView(true);
		WorkflowRequestTableField[1].setEdit(true);
		
		WorkflowRequestTableField[2] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[2].setFieldName("amt");//部门
		WorkflowRequestTableField[2].setFieldValue("23.00");
		WorkflowRequestTableField[2].setView(true);
		WorkflowRequestTableField[2].setEdit(true);

		WorkflowRequestTableField[3] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[3].setFieldName("srm");//文档
		WorkflowRequestTableField[3].setFieldValue("");
		WorkflowRequestTableField[3].setView(true);
		WorkflowRequestTableField[3].setEdit(true);
		
		WorkflowRequestTableField[4] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[4].setFieldName("textare");//备注
		WorkflowRequestTableField[4].setFieldValue("测试");
		WorkflowRequestTableField[4].setView(true);
		WorkflowRequestTableField[4].setEdit(true);
		
		WorkflowRequestTableField[5] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[5].setFieldName("fujian");//附件
		WorkflowRequestTableField[5].setFieldType("http:baidu_sylogo1.gif");//http:开头代表该字段为附件字段		多附件 用 | 分隔
		WorkflowRequestTableField[5].setFieldValue("http://pic2.ooopic.com/01/03/51/25b1OOOPIC19.jpg|http://pic.nipic.com/2007-11-09/200711912453162_2.jpg");
		WorkflowRequestTableField[5].setView(true);
		WorkflowRequestTableField[5].setEdit(true);

		workflowRequestTableRecord[0] = new WorkflowRequestTableRecord();
		workflowRequestTableRecord[0].setWorkflowRequestTableFields(WorkflowRequestTableField);
		workflowMainTableInfo.setRequestRecords(workflowRequestTableRecord);

		workflowRequestInfo.setWorkflowMainTableInfo(workflowMainTableInfo);
		/****************main table end*************/


		/****************detail table start*************/
		WorkflowDetailTableInfo[] workflowDetailTableInfo = new WorkflowDetailTableInfo[2];//两个明细表
		/**********第一张明细表开始**********/
		workflowRequestTableRecord = new WorkflowRequestTableRecord[2];//两行数据（两条记录）
		WorkflowRequestTableField = new WorkflowRequestTableField[2];//每行2个字段
		/****第一行开始****/
		WorkflowRequestTableField[0] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[0].setFieldName("type");//select框
		WorkflowRequestTableField[0].setFieldValue("0");
		WorkflowRequestTableField[0].setView(true);
		WorkflowRequestTableField[0].setEdit(true);	

		WorkflowRequestTableField[1] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[1].setFieldName("checking");//check框
		WorkflowRequestTableField[1].setFieldValue("0");
		WorkflowRequestTableField[1].setView(true);
		WorkflowRequestTableField[1].setEdit(true);
		workflowRequestTableRecord[0] = new WorkflowRequestTableRecord();
		workflowRequestTableRecord[0].setWorkflowRequestTableFields(WorkflowRequestTableField);
		/****第一行结束****/

		/****第二行开始****/
		WorkflowRequestTableField = new WorkflowRequestTableField[2];//每行2个字段
		WorkflowRequestTableField[0] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[0].setFieldName("type");//select框
		WorkflowRequestTableField[0].setFieldValue("1");
		WorkflowRequestTableField[0].setView(true);
		WorkflowRequestTableField[0].setEdit(true);	

		WorkflowRequestTableField[1] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[1].setFieldName("checking");//check框
		WorkflowRequestTableField[1].setFieldValue("1");
		WorkflowRequestTableField[1].setView(true);
		WorkflowRequestTableField[1].setEdit(true);
		workflowRequestTableRecord[1] = new WorkflowRequestTableRecord();
		workflowRequestTableRecord[1].setWorkflowRequestTableFields(WorkflowRequestTableField);
		/****第二行结束****/
		workflowDetailTableInfo[0] =new WorkflowDetailTableInfo();
		workflowDetailTableInfo[0].setWorkflowRequestTableRecords(workflowRequestTableRecord);
		/**********第一张明细表结束**********/

		/**********第二张明细表开始**********/
		workflowRequestTableRecord = new WorkflowRequestTableRecord[1];//一行数据（一条记录）
		WorkflowRequestTableField = new WorkflowRequestTableField[3];//每行3个字段
		/****第一行开始****/
		WorkflowRequestTableField[0] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[0].setFieldName("test1");//测试
		WorkflowRequestTableField[0].setFieldValue("test1");
		WorkflowRequestTableField[0].setView(true);
		WorkflowRequestTableField[0].setEdit(true);	

		WorkflowRequestTableField[1] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[1].setFieldName("test2");//测试
		WorkflowRequestTableField[1].setFieldValue("test2");
		WorkflowRequestTableField[1].setView(true);
		WorkflowRequestTableField[1].setEdit(true);
		workflowRequestTableRecord[0] = new WorkflowRequestTableRecord();
		workflowRequestTableRecord[0].setWorkflowRequestTableFields(WorkflowRequestTableField);

		WorkflowRequestTableField[2] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[2].setFieldName("test3");//测试
		WorkflowRequestTableField[2].setFieldValue("test3");
		WorkflowRequestTableField[2].setView(true);
		WorkflowRequestTableField[2].setEdit(true);
		workflowRequestTableRecord[0] = new WorkflowRequestTableRecord();
		workflowRequestTableRecord[0].setWorkflowRequestTableFields(WorkflowRequestTableField);
		/****第一行结束****/

		workflowDetailTableInfo[1] =new WorkflowDetailTableInfo();
		workflowDetailTableInfo[1].setWorkflowRequestTableRecords(workflowRequestTableRecord);
		/**********第二张明细表结束**********/

		workflowRequestInfo.setWorkflowDetailTableInfos(workflowDetailTableInfo);
		/****************detail table end*************/

//		String response = ClientUtil.getClient().submitWorkflowRequest(workflowRequestInfo, requestid, userid, type, remark);

//		if(!"".equals(response)&&response!=null)
//		System.out.println("返回结果："+response);
//		else
//		System.out.println("返回结果为空");
		String response = ClientUtil.getClient().doCreateWorkflowRequest(workflowRequestInfo, userid);
		System.out.println("requestid:" + response);
	}


}
