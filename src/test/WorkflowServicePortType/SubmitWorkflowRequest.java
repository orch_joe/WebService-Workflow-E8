package test.WorkflowServicePortType;
import java.rmi.RemoteException;

import org.junit.Test;

import weaver.workflow.webservices.WorkflowBaseInfo;
import weaver.workflow.webservices.WorkflowDetailTableInfo;
import weaver.workflow.webservices.WorkflowMainTableInfo;
import weaver.workflow.webservices.WorkflowRequestInfo;
import weaver.workflow.webservices.WorkflowRequestTableField;
import weaver.workflow.webservices.WorkflowRequestTableRecord;

/**
 * 提交流程
 * @author zxd
 *
 */
public class SubmitWorkflowRequest {

	/**
	 * 流程提交
	 * 
	 * @param WorkflowRequestInfo 流程信息
	 * @param requestid 流程请求ID
	 * @param userid 当前用户
	 * @param type 提交类型
	 * @param remark 签字意见
	 * @return String 提交结果
	 */
	@Test
	public void excute() throws RemoteException{
		WorkflowRequestInfo workflowRequestInfo = new WorkflowRequestInfo();//工作流程请求信息

		int requestid = 1919896;
		int userid = 229;//司琪
		String type = "submit";//submit 提交 subnoback提交不需返回  subback提交需要返回 reject 退回
		String remark = "OK-test";
		workflowRequestInfo.setRequestId("1919896");//流程请求ID 创建时不需要传-提交修改时需要传
		workflowRequestInfo.setCanView(true);//显示
		workflowRequestInfo.setCanEdit(true);//可编辑
		workflowRequestInfo.setRequestName("流程请求标题-webservice-test");//请求标题
		workflowRequestInfo.setRequestLevel("0");//请求重要级别  0：正常 1：重要 2：紧急
		workflowRequestInfo.setCreatorId("115");//创建者id
		workflowRequestInfo.setStatus("1");
		WorkflowBaseInfo workflowBaseInfo = new WorkflowBaseInfo();//工作流信息
		workflowBaseInfo.setWorkflowId("14679");//流程ID
		workflowBaseInfo.setWorkflowName("webservice-test");//流程名称
		workflowBaseInfo.setWorkflowTypeId("1951");//流程类型id
		workflowBaseInfo.setWorkflowTypeName("webservice-test");//流程类型名称
		workflowRequestInfo.setWorkflowBaseInfo(workflowBaseInfo);//工作流信息

		/****************main table start*************/
		WorkflowMainTableInfo workflowMainTableInfo = new WorkflowMainTableInfo();//主表
		WorkflowRequestTableRecord[] workflowRequestTableRecord = new WorkflowRequestTableRecord[1];//主表字段只有一条记录
		WorkflowRequestTableField[] WorkflowRequestTableField = new WorkflowRequestTableField[6];//主的4个字段

		WorkflowRequestTableField[0] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[0].setFieldName("name");//姓名
		WorkflowRequestTableField[0].setFieldValue("115");//被留言人字段的值，111为被留言人id
		WorkflowRequestTableField[0].setView(true);//字段是否可见
		WorkflowRequestTableField[0].setEdit(true);//字段是否可编辑

		WorkflowRequestTableField[1] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[1].setFieldName("department");//部门
		WorkflowRequestTableField[1].setFieldValue("3");
		WorkflowRequestTableField[1].setView(true);
		WorkflowRequestTableField[1].setEdit(true);
		
		WorkflowRequestTableField[2] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[2].setFieldName("amt");//部门
		WorkflowRequestTableField[2].setFieldValue("23.00");
		WorkflowRequestTableField[2].setView(true);
		WorkflowRequestTableField[2].setEdit(true);

		WorkflowRequestTableField[3] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[3].setFieldName("srm");//文档
		WorkflowRequestTableField[3].setFieldValue("");
		WorkflowRequestTableField[3].setView(true);
		WorkflowRequestTableField[3].setEdit(true);
		
		WorkflowRequestTableField[4] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[4].setFieldName("textare");//备注
		WorkflowRequestTableField[4].setFieldValue("测试");
		WorkflowRequestTableField[4].setView(true);
		WorkflowRequestTableField[4].setEdit(true);
		
		WorkflowRequestTableField[5] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[5].setFieldName("fujian");//附件
		WorkflowRequestTableField[5].setFieldType("http:baidu_sylogo1.gif");//http:开头代表该字段为附件字段		多附件 用 | 分隔
		WorkflowRequestTableField[5].setFieldValue("http://pic2.ooopic.com/01/03/51/25b1OOOPIC19.jpg|http://pic.nipic.com/2007-11-09/200711912453162_2.jpg");
		WorkflowRequestTableField[5].setView(true);
		WorkflowRequestTableField[5].setEdit(true);

		workflowRequestTableRecord[0] = new WorkflowRequestTableRecord();
		workflowRequestTableRecord[0].setWorkflowRequestTableFields(WorkflowRequestTableField);
		workflowMainTableInfo.setRequestRecords(workflowRequestTableRecord);

		workflowRequestInfo.setWorkflowMainTableInfo(workflowMainTableInfo);
		/****************main table end*************/


		/****************detail table start*************/
		WorkflowDetailTableInfo[] workflowDetailTableInfo = new WorkflowDetailTableInfo[2];//两个明细表
		/**********第一张明细表开始**********/
		workflowRequestTableRecord = new WorkflowRequestTableRecord[2];//两行数据（两条记录）
		WorkflowRequestTableField = new WorkflowRequestTableField[2];//每行2个字段
		/****第一行开始****/
		WorkflowRequestTableField[0] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[0].setFieldName("type");//select框
		WorkflowRequestTableField[0].setFieldValue("0");
		WorkflowRequestTableField[0].setView(true);
		WorkflowRequestTableField[0].setEdit(true);	

		WorkflowRequestTableField[1] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[1].setFieldName("checking");//check框
		WorkflowRequestTableField[1].setFieldValue("0");
		WorkflowRequestTableField[1].setView(true);
		WorkflowRequestTableField[1].setEdit(true);
		workflowRequestTableRecord[0] = new WorkflowRequestTableRecord();
		workflowRequestTableRecord[0].setWorkflowRequestTableFields(WorkflowRequestTableField);
		/****第一行结束****/

		/****第二行开始****/
		WorkflowRequestTableField = new WorkflowRequestTableField[2];//每行2个字段
		WorkflowRequestTableField[0] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[0].setFieldName("type");//select框
		WorkflowRequestTableField[0].setFieldValue("1");
		WorkflowRequestTableField[0].setView(true);
		WorkflowRequestTableField[0].setEdit(true);	

		WorkflowRequestTableField[1] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[1].setFieldName("checking");//check框
		WorkflowRequestTableField[1].setFieldValue("1");
		WorkflowRequestTableField[1].setView(true);
		WorkflowRequestTableField[1].setEdit(true);
		workflowRequestTableRecord[1] = new WorkflowRequestTableRecord();
		workflowRequestTableRecord[1].setWorkflowRequestTableFields(WorkflowRequestTableField);
		/****第二行结束****/
		workflowDetailTableInfo[0] =new WorkflowDetailTableInfo();
		workflowDetailTableInfo[0].setWorkflowRequestTableRecords(workflowRequestTableRecord);
		/**********第一张明细表结束**********/

		/**********第二张明细表开始**********/
		workflowRequestTableRecord = new WorkflowRequestTableRecord[1];//一行数据（一条记录）
		WorkflowRequestTableField = new WorkflowRequestTableField[3];//每行3个字段
		/****第一行开始****/
		WorkflowRequestTableField[0] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[0].setFieldName("test1");//测试
		WorkflowRequestTableField[0].setFieldValue("test1");
		WorkflowRequestTableField[0].setView(true);
		WorkflowRequestTableField[0].setEdit(true);	

		WorkflowRequestTableField[1] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[1].setFieldName("test2");//测试
		WorkflowRequestTableField[1].setFieldValue("test2");
		WorkflowRequestTableField[1].setView(true);
		WorkflowRequestTableField[1].setEdit(true);
		workflowRequestTableRecord[0] = new WorkflowRequestTableRecord();
		workflowRequestTableRecord[0].setWorkflowRequestTableFields(WorkflowRequestTableField);

		WorkflowRequestTableField[2] = new WorkflowRequestTableField(); 
		WorkflowRequestTableField[2].setFieldName("test3");//测试
		WorkflowRequestTableField[2].setFieldValue("test3");
		WorkflowRequestTableField[2].setView(true);
		WorkflowRequestTableField[2].setEdit(true);
		workflowRequestTableRecord[0] = new WorkflowRequestTableRecord();
		workflowRequestTableRecord[0].setWorkflowRequestTableFields(WorkflowRequestTableField);
		/****第一行结束****/

		workflowDetailTableInfo[1] =new WorkflowDetailTableInfo();
		workflowDetailTableInfo[1].setWorkflowRequestTableRecords(workflowRequestTableRecord);
		/**********第二张明细表结束**********/

		workflowRequestInfo.setWorkflowDetailTableInfos(workflowDetailTableInfo);
		/****************detail table end*************/



		String response = ClientUtil.getClient().submitWorkflowRequest(workflowRequestInfo, requestid, userid, type, remark);

		if(!"".equals(response)&&response!=null)
			System.out.println("返回结果response："+response);
		else
			System.out.println("返回结果为空");
	}


	/**
	 * 通过查询出流程详细提交流程
	 * @throws RemoteException
	 */
	@Test
	public void excute1() throws RemoteException{
		int requestid = 1919150;
		int userid = 229;//司琪
		String type = "submit";//submit 提交 subnoback提交不需返回  subback提交需要返回 reject
		String remark = "OK";
		WorkflowRequestInfo workflowRequestInfo  = ClientUtil.getClient().getWorkflowRequest(requestid, userid, requestid);
		String response = ClientUtil.getClient().submitWorkflowRequest(workflowRequestInfo, requestid, userid, type, remark);
		if(!"".equals(response)&&response!=null)
			System.out.println("返回结果response1："+response);
		else
			System.out.println("返回结果为空");
	}
}
