package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;
/**
 * 获取我的请求数量
 * @author Administrator
 *
 */
public class GetMyWorkflowRequestCount {
	
	/**
	 * 获取我的请求数量
	 * 
	 * @param userid 当前用户
	 * @param conditions 查询条件
	 * @return int 待办流程数量
	 */
	@Test
	public void excute() throws RemoteException{
		int userid = 1;//当前用户
		String[] conditions = new String[2];//查询条件 两个

		int response =  ClientUtil.getClient().getMyWorkflowRequestCount(userid, conditions);

		System.out.println("我的请求数量:"+response);
	}
}
