package test.WorkflowServicePortType;
import java.rmi.RemoteException;
import org.junit.Test;
import weaver.workflow.webservices.WorkflowBaseInfo;

/**
 * 取得可创建的工作流列表示例
 * @author zxd
 *
 */
public class GetCreateWorkflowList {
	
	
	/**
	 * 取得可创建的工作流列表
	 * 
	 * @param pageNo 当前页数
	 * @param pageSize 每页记录数
	 * @param recordCount 记录总数
	 * @param userid 当前用户
	 * @param workflowType 工作流类型
	 * @param conditions 查询条件
	 * @return WorkflowBaseInfo 工作流列表
	 */
	@Test
	public void excute() throws RemoteException{
		int pageNo = 1;//当前页数
		int pageSize = 20;//每页记录数
		int recordCount = 1000;//记录总数
		int userid = 1;//当前用户
		int workflowType = 0;//工作流类型
		String[] conditions = new String[2];//查询条件
		conditions[0] = "";
		conditions[1] = "";
		WorkflowBaseInfo[] response = ClientUtil.getClient().getCreateWorkflowList(pageNo, pageSize, recordCount, userid, workflowType, conditions);
		System.out.println("length:"+response.length);
		for (int i = 0; i < response.length; i++) {
			System.out.println("流程名称:"+response[i].getWorkflowName()+
					" ---"+"流程id:"+response[i].getWorkflowId()+
					" ---"+"流程类型:"+response[i].getWorkflowTypeId()+
					" ---"+"流程类型:"+response[i].getWorkflowTypeName());
		}
	}
}
