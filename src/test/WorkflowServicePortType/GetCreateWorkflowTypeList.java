package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;

import weaver.workflow.webservices.WorkflowBaseInfo;

/**
 * 取得可创建的工作流类型列表
 * @author Administrator
 *
 */
public class GetCreateWorkflowTypeList {
	
	
	/**
	 * 取得可创建的工作流类型列表
	 * 
	 * @param pageNo 当前页数
	 * @param pageSize 每页记录数
	 * @param recordCount 记录总数
	 * @param userid 当前用户
	 * @param conditions 查询条件
	 * @return WorkflowBaseInfo 工作流类型列表
	 */
	@Test
	public void excute() throws RemoteException{
		int pageNo = 2;//当前页数
		int pageSize = 10;//每页记录数
		int recordCount = 1000;//记录总数
		int userid = 0;//当前用户
		String[] conditions = new String[2];//查询条件
		conditions[0] = "";
		conditions[1] = "";
		WorkflowBaseInfo[] response = ClientUtil.getClient().getCreateWorkflowTypeList(pageNo, pageSize, recordCount, userid, conditions);

		System.out.println("length:"+response.length);

		for (WorkflowBaseInfo workflowBaseInfo : response) {
			System.out.println("流程名称:"+workflowBaseInfo.getWorkflowName()+
					" -----"+"流程id:"+workflowBaseInfo.getWorkflowId()+
					" -----"+"流程类型名称:"+workflowBaseInfo.getWorkflowTypeName());
		}

	}
}
