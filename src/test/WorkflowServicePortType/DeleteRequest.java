package test.WorkflowServicePortType;
import java.rmi.RemoteException;
import org.junit.Test;
import cn.com.weaver.services.webservices.WorkflowServicePortTypeProxy;

/**
 * 删除请求
 * @author zxd
 *
 */
public class DeleteRequest {

	private static final String WSURL = "http://localhost:8080/services/WorkflowService";
	private WorkflowServicePortTypeProxy client;

	public DeleteRequest() {
		this.client = new WorkflowServicePortTypeProxy(WSURL);
	}
	
	/**
	 * 删除流程
	 * @param requestid 请求id
	 * @param userId 用户id
	 * @return 删除是否成功 true成功，false失败
	 */
	@Test
	public void excute() throws RemoteException{
		
		int requestid = 0;//请求id
		int userId = 0;//用户id
		boolean response = client.deleteRequest(requestid, userId);
		
		System.out.println("response"+String.valueOf(response));
	}
}
