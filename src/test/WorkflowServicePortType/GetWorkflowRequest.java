package test.WorkflowServicePortType;

import org.junit.Test;

import cn.com.weaver.services.webservices.WorkflowServicePortTypeProxy;

import weaver.workflow.webservices.WorkflowBaseInfo;
import weaver.workflow.webservices.WorkflowDetailTableInfo;
import weaver.workflow.webservices.WorkflowMainTableInfo;
import weaver.workflow.webservices.WorkflowRequestInfo;
import weaver.workflow.webservices.WorkflowRequestLog;
import weaver.workflow.webservices.WorkflowRequestTableField;
import weaver.workflow.webservices.WorkflowRequestTableRecord;
/**
 * 取得流程详细信息
 * @author Administrator
 *
 */
public class GetWorkflowRequest {

	private static final String          wsurl = "http://localhost:8080/services/WorkflowService";
	private WorkflowServicePortTypeProxy client;

	public GetWorkflowRequest() {
		this.client = new WorkflowServicePortTypeProxy(wsurl);
	}

	/**
	 * 取得流程详细信息
	 * 
	 * @param requestid 流程请求ID
	 * @param userid 当前用户
	 * @return WorkflowRequestInfo 流程信息
	 */
	@Test
	public void excute() throws Exception {

		int requestid = 1918669;//流程请求ID
		int userid = 115;//当前用户
		int fromrequestid = 1918669;//原流程id
		WorkflowRequestInfo wri = client.getWorkflowRequest(requestid, userid, fromrequestid);
		WorkflowRequestLog[] logs = wri.getWorkflowRequestLogs();

		for (int i = 0; i < logs.length; i++) {
			WorkflowRequestLog log = logs[i];
			String Remark = log.getRemark();
			String OperateDate = log.getOperateDate();
			String OperateTime = log.getOperateTime();
			String OperatorId = log.getOperatorId();
			String OperatorName = log.getOperatorName();

			System.out.println("OperatorId=" + OperatorId + ", OperatorName=" + OperatorName + ", Remark=" + Remark
					+ ", OperateDate=" + OperateDate + ", OperateTime=" + OperateTime);

		}

		/*******解析工作流程base信息*****/
		WorkflowBaseInfo  WorkflowBaseInfo = wri.getWorkflowBaseInfo();
		System.out.println(" -----"+"流程类型id："+ WorkflowBaseInfo.getWorkflowTypeId()+
				" -----"+"流程名称："+ WorkflowBaseInfo.getWorkflowName());

		/*******解析主表信息*****/
		WorkflowMainTableInfo workflowMainTableInfo = wri.getWorkflowMainTableInfo();
		WorkflowRequestTableRecord[] workflowRequestTableRecords = workflowMainTableInfo.getRequestRecords();
		for (WorkflowRequestTableRecord workflowRequestTableRecord : workflowRequestTableRecords) {
			WorkflowRequestTableField[] workflowRequestTableFields = workflowRequestTableRecord.getWorkflowRequestTableFields();
			for (WorkflowRequestTableField workflowRequestTableField : workflowRequestTableFields) {
				System.out.println("主表字段名称："+workflowRequestTableField.getFieldName()+
						"---："+workflowRequestTableField.getFieldValue());
			}
		}

		/*******解析明细表信息*****/
		WorkflowDetailTableInfo[] workflowDetailTableInfos =wri.getWorkflowDetailTableInfos();
		System.out.println(workflowDetailTableInfos.length);
		for (WorkflowDetailTableInfo workflowDetailTableInfo : workflowDetailTableInfos) {
			WorkflowRequestTableRecord[] workflowRequestTableRecords1 =  workflowDetailTableInfo.getWorkflowRequestTableRecords();
			for (WorkflowRequestTableRecord workflowRequestTableRecord : workflowRequestTableRecords1) {
				WorkflowRequestTableField[]  workflowRequestTableFields = workflowRequestTableRecord.getWorkflowRequestTableFields();
				int i=1;
				for (WorkflowRequestTableField workflowRequestTableField : workflowRequestTableFields) {
					System.out.println("明细"+i+"字段名称："+workflowRequestTableField.getFieldName());
				}
				i++;
			}
		}

	}
}
