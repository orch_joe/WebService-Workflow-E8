package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;

/**
 * 获取归档流程数量
 * @author Administrator
 *
 */
public class GetProcessedWorkflowRequestCount {
	
	/**
	 * 获取归档流程数量
	 * 
	 * @param userid 当前用户
	 * @param conditions 查询条件
	 * @return int 归档流程数量
	 */
	@Test
	public void excute() throws RemoteException{
		int userid = 1;//当前用户
		String[] conditions = new String[2];//查询条件

		int response =  ClientUtil.getClient().getProcessedWorkflowRequestCount(userid, conditions);

		System.out.println("归档流程数量:"+response);
	}
}
