package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;

public class WriteWorkflowReadFlag {
	
	/**
	 * 写入流程查看日志
	 * @param requestid
	 * @param userid
	 */
	@Test
	public void excute() throws RemoteException{
		String requestid = "1536255";//请求流程ID
		String userid = "1";//当前用户
		ClientUtil.getClient().writeWorkflowReadFlag(requestid, userid);
	}
}
