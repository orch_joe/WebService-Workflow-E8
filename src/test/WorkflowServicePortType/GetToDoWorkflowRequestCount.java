package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;
/**
 * 获取待办流程数量
 * @author Administrator
 *
 */
public class GetToDoWorkflowRequestCount {
	
	
	/**
	 * 获取待办流程数量
	 * 
	 * @param userid 当前用户
	 * @param conditions 查询条件
	 * @return int 待办流程数量
	 */
	@Test
	public void excute() throws RemoteException{
		int userid = 1;//用户ID
		String[] conditions = new String[2];//查询条件

		int response =  ClientUtil.getClient().getToDoWorkflowRequestCount(userid, conditions);

		System.out.println("length:"+response);

	}
}
