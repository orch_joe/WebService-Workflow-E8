package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;

import weaver.workflow.webservices.WorkflowBaseInfo;
import weaver.workflow.webservices.WorkflowDetailTableInfo;
import weaver.workflow.webservices.WorkflowMainTableInfo;
import weaver.workflow.webservices.WorkflowRequestInfo;
import weaver.workflow.webservices.WorkflowRequestTableField;
import weaver.workflow.webservices.WorkflowRequestTableRecord;

import cn.com.weaver.services.webservices.WorkflowServicePortTypeProxy;


/**
 * 搜索所有可用流程示例
 * @author zxd
 *
 */
public class GetAllWorkflowRequestList {

	private static final String WSURL = "http://localhost:8080/services/WorkflowService";
	private WorkflowServicePortTypeProxy client;

	public GetAllWorkflowRequestList() {
		this.client = new WorkflowServicePortTypeProxy(WSURL);
	}

	/**
	 * 搜索所有可用流程
	 * @param pageNo 当前页数
	 * @param pageSize 每页记录数
	 * @param recordCount 记录总数
	 * @param keyword 流程标题
	 * @param userid 当前用户
	 * @param conditions 查询条件
	 * @return WorkflowRequestInfo 所有可用流程信息
	 */
	@Test
	public void excute() throws RemoteException{

		int pageNo = 1;//当前页数
		int pageSize = 10;//每页记录数
		int recordCount = 1000;//记录总数
		int userid = 115;//当前用户
		String[] conditions = new String[2];//查询条件
		WorkflowRequestInfo[] response = client.getAllWorkflowRequestList(pageNo, pageSize, recordCount, userid, conditions);
		System.out.println("length:"+response.length);
		for (WorkflowRequestInfo workflowRequestInfo : response) {
			System.out.println("是否可编辑："+workflowRequestInfo.getCanEdit()+
					" -----"+"当前节点名称:"+workflowRequestInfo.getCurrentNodeName()+
					" -----"+"创建者："+workflowRequestInfo.getCreatorName());

			/*******解析工作流程base信息*****/
			WorkflowBaseInfo  WorkflowBaseInfo = workflowRequestInfo.getWorkflowBaseInfo();
			System.out.println(" -----"+"流程类型id："+ WorkflowBaseInfo.getWorkflowTypeId()+
					" -----"+"流程名称："+ WorkflowBaseInfo.getWorkflowName());

			/*******解析主表信息*****/
			WorkflowMainTableInfo workflowMainTableInfo = workflowRequestInfo.getWorkflowMainTableInfo();

			if(workflowMainTableInfo != null){

				WorkflowRequestTableRecord[] workflowRequestTableRecords = workflowMainTableInfo.getRequestRecords();
				for (WorkflowRequestTableRecord workflowRequestTableRecord : workflowRequestTableRecords) {
					WorkflowRequestTableField[] workflowRequestTableFields = workflowRequestTableRecord.getWorkflowRequestTableFields();
					for (WorkflowRequestTableField workflowRequestTableField : workflowRequestTableFields) {
						System.out.println("字段名称："+workflowRequestTableField.getFieldName());
					}
				}
			}

			/*******解析明细表信息*****/
			WorkflowDetailTableInfo[] workflowDetailTableInfos =workflowRequestInfo.getWorkflowDetailTableInfos();
			if(workflowDetailTableInfos.length != 0){

				for (WorkflowDetailTableInfo workflowDetailTableInfo : workflowDetailTableInfos) {
					WorkflowRequestTableRecord[] workflowRequestTableRecords1 =  workflowDetailTableInfo.getWorkflowRequestTableRecords();
					for (WorkflowRequestTableRecord workflowRequestTableRecord : workflowRequestTableRecords1) {
						WorkflowRequestTableField[]  workflowRequestTableFields = workflowRequestTableRecord.getWorkflowRequestTableFields();
						System.out.println("明细表个数："+workflowRequestTableFields.length);
						for (WorkflowRequestTableField workflowRequestTableField : workflowRequestTableFields) {

							System.out.println("字段名称："+workflowRequestTableField.getFieldName());
						}
					}
				}
			}

		}
	}
}
