package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;

import cn.com.weaver.services.webservices.WorkflowServicePortTypeProxy;

/**
 *  流程转发示例
 * @author Administrator
 *
 */
public class ForwardWorkflowRequest{
	private static final String WSURL = "http://localhost:8080/services/WorkflowService";
	private WorkflowServicePortTypeProxy client;

	public ForwardWorkflowRequest() {
		this.client = new WorkflowServicePortTypeProxy(WSURL);
	}
	
	/**
	 * 流程转发
	 * 
	 * @param requestid 流程请求ID
	 * @param recipients 转发人
	 * @param userid 当前用户
	 * @param remark 签字意见
	 * @param clientip 用户IP
	 * @return String 转发结果
	 */
	@Test
	public void excute() throws RemoteException{
		
		int requestid = 0;//流程请求ID
		String recipients = "123";//转发人
		String remark = "56";//签字意见
		int userid = 1;//当前用户
		String clientip = "456";//用户IP
		
		String response = client.forwardWorkflowRequest(requestid, recipients, remark, userid, clientip);
		
		System.out.println("response:"+response);
	}
}
