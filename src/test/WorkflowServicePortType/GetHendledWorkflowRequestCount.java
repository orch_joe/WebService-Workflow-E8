package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;
/**
 * 获取已办流程数量示例
 * @author Administrator
 *
 */
public class GetHendledWorkflowRequestCount {
	
	/**
	 * 获取已办流程数量
	 * 
	 * @param userid 当前用户
	 * @param conditions 查询条件
	 * @return int 已办流程数量
	 */
	@Test
	public void excute() throws RemoteException{
		int userid = 1;//当前用户
		String[] conditions = new String[2];//查询条件
		conditions[0] = "";
		conditions[1] = "";
		int response = ClientUtil.getClient().getHendledWorkflowRequestCount(userid, conditions);
		
		System.out.println("已办流程数量:"+response);
	}
}
