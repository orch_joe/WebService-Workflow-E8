package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;

import weaver.workflow.webservices.WorkflowRequestLog;

public class GetWorkflowRequestLogs {
	
	/**
	 * 取得流程详细信息
	 * 
	 * @param workflowId 流程ID
	 * @param requestId 流程请求ID
	 * @param userid 用户
	 * @param pagesize 分页条数
	 * @param endId 结束ID
	 * @return WorkflowRequestLog 流程信息
	 */
	@Test
	public void excute() throws RemoteException{
		String workflowId = "14679";//流程ID
		String requestId = "1536255";//流程请求ID
		int userid = 1;//用户
		int pagesize = 10;//分页条数
		int endId = 100;//结束ID
		WorkflowRequestLog[] response =  ClientUtil.getClient().getWorkflowRequestLogs(workflowId, requestId, userid, pagesize,endId);
		System.out.println("length:"+response.length);
		for (WorkflowRequestLog workflowRequestLog : response) {
			System.out.println("OperatorName"+workflowRequestLog.getOperatorName());
		}

	}
}
