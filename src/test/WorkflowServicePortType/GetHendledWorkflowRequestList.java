package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;

import weaver.workflow.webservices.WorkflowBaseInfo;
import weaver.workflow.webservices.WorkflowDetailTableInfo;
import weaver.workflow.webservices.WorkflowMainTableInfo;
import weaver.workflow.webservices.WorkflowRequestInfo;
import weaver.workflow.webservices.WorkflowRequestTableField;
import weaver.workflow.webservices.WorkflowRequestTableRecord;
/**
 * 获取已办流程列表示例
 * @author Administrator
 *
 */
public class GetHendledWorkflowRequestList {

	/**
	 * 获取已办流程列表
	 * 
	 * @param pageNo 当前页数
	 * @param pageSize 每页记录数
	 * @param recordCount 记录总数
	 * @param userid 当前用户
	 * @param conditions 查询条件
	 * @return WorkflowRequestInfo 已办流程信息
	 */
	@Test
	public void excute() throws RemoteException{
		int pageNo = 1; //当前页数
		int pageSize = 10; //每页记录数
		int recordCount = 1000; //每页记录数 可通过getHandledWorkflowRequestCount查询到总记录数
		int userid = 1;// 当前用户
		String[] conditions = new String[2];//查询条件
		conditions[0] = "";
		conditions[1] = "";
		WorkflowRequestInfo[] response = ClientUtil.getClient().getHendledWorkflowRequestList(pageNo, pageSize, recordCount, userid, conditions);

		System.out.println("length:"+response.length);
		for (WorkflowRequestInfo workflowRequestInfo : response) {
			System.out.println("是否可编辑："+workflowRequestInfo.getCanEdit()+
					" -----"+"当前节点名称:"+workflowRequestInfo.getCurrentNodeName()+
					" -----"+"创建者："+workflowRequestInfo.getCreatorName());

			/*******解析工作流程base信息*****/
			WorkflowBaseInfo  WorkflowBaseInfo = workflowRequestInfo.getWorkflowBaseInfo();
			System.out.println(" -----"+"流程类型id："+ WorkflowBaseInfo.getWorkflowTypeId()+
					" -----"+"流程名称："+ WorkflowBaseInfo.getWorkflowName());

			/*******解析主表信息*****/
			WorkflowMainTableInfo workflowMainTableInfo = workflowRequestInfo.getWorkflowMainTableInfo();
			WorkflowRequestTableRecord[] workflowRequestTableRecords = workflowMainTableInfo.getRequestRecords();
			for (WorkflowRequestTableRecord workflowRequestTableRecord : workflowRequestTableRecords) {
				WorkflowRequestTableField[] workflowRequestTableFields = workflowRequestTableRecord.getWorkflowRequestTableFields();
				for (WorkflowRequestTableField workflowRequestTableField : workflowRequestTableFields) {
					System.out.println("字段名称："+workflowRequestTableField.getFieldName());
				}
			}

			/*******解析明细表信息*****/
			WorkflowDetailTableInfo[] workflowDetailTableInfos =workflowRequestInfo.getWorkflowDetailTableInfos();
			for (WorkflowDetailTableInfo workflowDetailTableInfo : workflowDetailTableInfos) {
				WorkflowRequestTableRecord[] workflowRequestTableRecords1 =  workflowDetailTableInfo.getWorkflowRequestTableRecords();
				for (WorkflowRequestTableRecord workflowRequestTableRecord : workflowRequestTableRecords1) {
					WorkflowRequestTableField[]  workflowRequestTableFields = workflowRequestTableRecord.getWorkflowRequestTableFields();
					for (WorkflowRequestTableField workflowRequestTableField : workflowRequestTableFields) {

						System.out.println("字段名称："+workflowRequestTableField.getFieldName());
					}
				}
			}

		}
	}
}
