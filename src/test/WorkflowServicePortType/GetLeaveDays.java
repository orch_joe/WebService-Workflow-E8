package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;
/**
 * 请假申请单特殊处理
 * @author Administrator
 *
 */
public class GetLeaveDays {
	
	/**
	 * 请假申请单特殊处理
	 * 根据起始日期、起始时间、结束日期、结束时间获得请假天数
	 * @param fromDate
	 * @param fromTime
	 * @param toDate
	 * @param toTime
	 * @param resourceId
	 * @return String 请假天数
	 */
	@Test
	public void excute() throws RemoteException{
		String fromDate = "20160113";//开始日期
		String fromTime = "";//开始时间
		String toDate = "20160114";//结束日期
		String toTime = "";//结束时间
		String resourceId = "12";//资源id

		String response =  ClientUtil.getClient().getLeaveDays(fromDate, fromTime, toDate, toTime, resourceId);

		System.out.println("response:"+response);
	}
}
