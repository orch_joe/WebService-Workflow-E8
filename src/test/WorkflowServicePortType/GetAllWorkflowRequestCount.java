package test.WorkflowServicePortType;
import org.junit.Test;
import cn.com.weaver.services.webservices.WorkflowServicePortTypeProxy;

/**
 * 搜索所有可用流程数量
 * @author zxd
 *
 */
public class GetAllWorkflowRequestCount {
	private static final String          wsurl = "http://localhost:8080/services/WorkflowService";
	private WorkflowServicePortTypeProxy client;

	public GetAllWorkflowRequestCount() {
		this.client = new WorkflowServicePortTypeProxy(wsurl);
	}

	/**
	 * 搜索所有可用流程数量
	 * @param userid 当前用户
	 * @param conditions 查询条件
	 * @return int 所有可用流程数量
	 */
	@Test
	public void excute() throws Exception{
		int userid = 115;//当前用户
		String[] conditions = new String[2];//增加两个查询条件
		conditions[0] = " t1.workflowid = 14679 ";//增加查询条件2 -- 工作流程类型id为1
		conditions[1] = " t1.currentnodetype = 2";//增加查询条件1 -- 当前节点类型为2
		int response = client.getAllWorkflowRequestCount(userid, conditions);
		System.out.println("response:"+response);

	}

}
