package test.WorkflowServicePortType;

import java.rmi.RemoteException;

import org.junit.Test;

import weaver.workflow.webservices.WorkflowRequestInfo;
/**
 * 
 * @author Administrator
 *
 */
public class GetWorkflowRequest4Split {
	
	/**
	 * 取得流程详细信息
	 * 
	 * @param requestid 流程请求ID
	 * @param userid 当前用户
	 * @param fromrequestid 前流程请求ID
	 * @param pagesize 签字意见的条数
	 * @return WorkflowRequestInfo 流程信息
	 */
	@Test
	public void excute() throws RemoteException{

		int requestid = 1536255;//流程请求ID
		int userid = 22175;//当前用户
		int fromrequestid = 1;//前流程请求ID
		int pagesize = 10;//签字意见的条数
		WorkflowRequestInfo response =  ClientUtil.getClient().getWorkflowRequest4Split(requestid, userid, fromrequestid, pagesize);
		System.out.println("创见者姓名:"+response.getCreatorName());

	}
}
